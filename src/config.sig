(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* User-specified runtime configuration *)

signature CONFIG =
sig
    type config

    val default : unit -> config
    val read : string -> config -> config

    val inPath : config -> string
    val outPath : config -> string
    val pubPath : config -> string
    val lib : config -> string
    val compiler : config -> string
    val cm : config -> string list
    val sml : config -> string
    val beforeT : config -> string option
    val afterT : config -> string option
    val exnT : config -> string option

    val printFn : config -> string -> string option
end