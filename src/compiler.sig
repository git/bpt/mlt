(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Template compiler interface *)

signature COMPILER =
sig
    exception Error

    val compileTemplate : Config.config * StaticEnv.staticEnv * StringSet.set -> string -> string * string
    (* [compileTemplate (env, fname, templates)] compiles the template stored in file fname in the environment env.
     * templates lists the templates that may be called. This returns the structure name used and the actual code. *)

    val compileDirectory : Config.config -> OS.Process.status
    (* [compileDirectory path] compiles all templates in directory path *)

    val compileTemplates : Config.config -> string list * string list -> OS.Process.status
    (* Compile a list of template files and SML files using the given config *)
end
